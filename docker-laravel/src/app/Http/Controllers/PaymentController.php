<?php

namespace App\Http\Controllers;

use App\Payment;
use Illuminate\Http\Request;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getList()
    {
        $items = array(
            'income_sum' => 45000,
            'expenditure_sum' =>  15000,
            'house_hold_list' => array(
                array(
                    'date' => '20200401',
                    'type' => 'income',
                    'name' => 'お給料',
                    'cadtegory' => '収入',
                    'money' => 45000
                ),
                array(
                    'date' => '20200402',
                    'type' => 'expend',
                    'name' => 'めっちゃいいお酒',
                    'cadtegory' => '娯楽',
                    'money' => 10000
                ),
                array(
                    'date' => '20200403',
                    'type' => 'expend',
                    'name' => '電車賃',
                    'cadtegory' => '交通費',
                    'money' => 5000
                )),
            'income-pie-chart' => array(
                    'category' => '収入',
                    'sum' => 45000
            ),
            'expenditure-pi-chart' => array(
                    array(
                        'category' => '娯楽',
                        'sum' => 10000
                    ),
                    array(
                        'category' => '電車賃',
                        'sum' => 5000
                    )
            )
            );
        return response()->json($items);
    }
}
